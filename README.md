Docker exemples
=========
Voici le repository git qui permet d'essayer Docker dans ses pires configurations.

## Exemples
- Dive
- Image scanning
- PrivEsc
- Registry abuse
- Docker Socket Local & Remote

> Regardez les fichiers "env" pour plus d'informations (ex: Dive/env)